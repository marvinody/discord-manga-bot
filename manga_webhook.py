#!/usr/bin/python3.5
import requests
import json
import re
import feedparser
from pprint import pprint
from datetime import datetime
from threading import Timer
import sys
import os
from bs4 import BeautifulSoup
from dotenv import load_dotenv
import time


def mangafreakxyz(url, index, series, lastChapter):
    global data
    r = requests.get(url)
    soup = BeautifulSoup(r.content, "lxml")
    chapterLIs = soup.find(
        'div', class_='chapter-list').find_all('li')
    for chapterLI in reversed(chapterLIs):
        anchor = chapterLI.find('a')
        reg = re.search(r'(\d+.*?)', anchor.text)
        if reg:
            fixed = reg.group(1).replace(",", ".")
            if float(fixed) > float(lastChapter):

                link = anchor.get('href')
                print(link)
                message = "Manga Update: `%s` Latest Chapter: %s\n%s" % (
                    series, fixed, link)
                send_message(message)
                data["entries"][index]["lastChapter"] = fixed
                lastChapter = fixed
                print("Updating Series: %s to %s" % (series, fixed))


def mangastream(rss, index, series, lastChapter):
    """Checks the rss stream for mangastream and see if there's an updated one"""
    global data

    for entry in rss.entries:
        if(series in entry.title):
            reg = re.search(r'(\d+.*?).*$', entry.title)
            if(reg):
                fixed = reg.group(1).replace(",", ".")
                if(float(fixed) > float(lastChapter)):
                    message = "Manga Update: `%s` Latest Chapter: %s\n%s" % (
                        series, fixed, entry.link)
                    send_message(message)
                    data["entries"][index]["lastChapter"] = fixed
                    lastChapter = fixed
                    print("Updating Series: %s to %s" % (series, fixed))


def mangapark(rssUrl, index, series, lastChapter):
    """
        Parses rss stream for particular series, checks if the most recent entry is newer
        Above is outdated but leaving it here to show that it used to be different
        MP changed their rss format to be like mangastream's and it fked me over
        Apparently, they changed it back to include single rss per manga...
    """
    global data
    rss = feedparser.parse(rssUrl)
    for entry in reversed(rss.entries):
        reg = re.search(r'ch\.(\d+.*?)(v2)?', entry.title)
        if(reg):
            fixed = reg.group(1).replace(",", ".")
            if(float(fixed) > float(lastChapter)):
                message = "Manga Update: `%s` Latest Chapter: %s\n%s" % (
                    series, fixed, entry.link)
                send_message(message)
                data["entries"][index]["lastChapter"] = fixed
                lastChapter = fixed
                print("Updating Series: %s to %s" % (series, fixed))


def mangadex(rssUrl, index, series, lastChapter):
    """
        Given the rss url for a certain manga, goes through it and does what the
        rest above do. Pretty simple. In this case, gotta check language tho

        This is a late comment, but I'm pretty sure this is the retarded site
        that uses a url to authenticate permanently. You need an account to login
        but you get some url to use over and over. They were complaining about
        scrapers pulling data, but then the scrapers just need to make a few
        accounts and get the value for each account. Then all the urls are
        created with that value. IDK. I'm just mad cause my shit broke and I didn't
        know for a while
    """
    global data
    rss = feedparser.parse(rssUrl)
    englishEntries = [e for e in rss.entries if 'English' in e.description]
    for entry in englishEntries:
        reg = re.search(r'Ch\w+ (\d+.*?)', entry.title)
        if reg:
            fixed = reg.group(1).replace(",", ".")
            if float(fixed) > float(lastChapter):
                message = "Manga Update: `%s` Latest Chapter: %s\n%s" % (
                    series, fixed, entry.link)
                send_message(message)
                data["entries"][index]["lastChapter"] = fixed
                lastChapter = fixed
                print("Updating Series: %s to %s" % (series, fixed))


def jaiminisbox(url, index, series, lastChapter):
    global data
    r = requests.get(url)
    soup = BeautifulSoup(r.content, "lxml")
    els = soup.find_all('div', 'element')
    chapterTitles = [list(el.children)[2].find('a').text for el in els]
    for idx, chapterTitle in enumerate(reversed(chapterTitles)):
        reg = re.search(r'(\d+.*?):', chapterTitle)
        if reg:
            fixed = reg.group(1).replace(",", ".")
            if float(fixed) > float(lastChapter):
                realIdx = len(els) - idx - 1
                link = els[realIdx].find_all('a')[1].get('href')
                message = "Manga Update: `%s` Latest Chapter: %s\n%s" % (
                    series, fixed, link)
                send_message(message)
                data["entries"][index]["lastChapter"] = fixed
                lastChapter = fixed
                print("Updating Series: %s to %s" % (series, fixed))


def send_message(message):
    global url
    payload = {'content': message, 'username': 'MANGA'}
    response = requests.post(url, payload)
    if response.status_code != 200 and response.status_code != 204:
        print("Error: ", response.text)
        jsonError = json.loads(response.text)
        sleepTime = jsonError['retry_after'] / 1000
        print("Sleeping for {}s".format(sleepTime))
        time.sleep(sleepTime+1)
        send_message(message)  # attempt sending again


def load_json():
    with open(json_file) as data_file:
        data = json.load(data_file)
        return data


def save_json():
    with open(json_file, "w") as outfile:
        json.dump(data, outfile, indent=4)


def check_new_data():

    print(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    ms_rss = feedparser.parse("http://mangastream.com/rss")
    mp_rss = feedparser.parse("http://mangapark.me/rss/latest.xml")
    global data

    index = -1
    for manga in data["entries"]:
        try:
            index += 1
            if("mangastream" in manga["updater"]):
                mangastream(ms_rss, index,
                            manga["series"], manga["lastChapter"])
            if("mangapark" in manga["updater"]):
                mangapark(manga['rssUrl'], index,
                          manga["series"], manga["lastChapter"])
            if("mangadex" in manga["updater"]):
                mangadex(manga['rssUrl'], index,
                         manga["series"], manga["lastChapter"])
            if("jaiminisbox" in manga["updater"]):
                jaiminisbox(manga['url'], index,
                            manga["series"], manga["lastChapter"])
            if("mangafreakxyz" in manga["updater"]):
                mangafreakxyz(manga['url'], index,
                              manga["series"], manga["lastChapter"])

        except Exception as e:
            print("Yo, shit broke for {}, {}: {}".format(
                manga['series'], type(e), e))
            send_message("Yo, shit broke for {}, {}".format(
                manga['series'], e))
        finally:
            save_json()
    save_json()
    print("Saving JSON")


json_file = fn = os.path.join(os.path.dirname(__file__), "manga.json")

load_dotenv()
url = os.getenv("DISCORD_URL")
data = load_json()
check_new_data()
