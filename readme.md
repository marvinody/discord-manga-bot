#Discord Manga Webhook Bot

Small script to check a file and see if new manga chapters have been found and post them to a discord channel if yes.
I do some silly stuff in it (global data manipulation, undocumented regexp) but it's a small script so I didn't care too much. I just wanted to know when One Piece chapters were released and then added more when friends wanted some

Setup is pretty easy, I think?
1. Clone it
2. Change the .env.sample -> .env and update the discord webhook url
3. Update or change manga.json with correct stuff (or not)
4. Install requirements with `pip install -r requirements.txt`
5. Setup in crontab or something to run it however often you want
  1. (Do not run this more than an hour is my recommendation. It's not nice to spam others' servers with requests)
6. Read manga when it comes out
